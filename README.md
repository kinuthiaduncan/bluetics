SETTING UP
-------------

##### Prerequisites
1. Install prerequisites 

    ```commandline
    sudo apt-get update $ sudo apt-get install python-pip python-dev build-essential libpq-dev postgresql postgresql-contrib virtualenv virtualenvwrapper $ export LC_ALL="en_US.UTF-8" $ pip install --upgrade pip $ pip install --upgrade virtualenv
    ```

##### PostgreSQL
1. Access  Postgres Prompt 

    ```commandline
    sudo -u postgres psql
    ```
2. Create the database 

    ```sql
    CREATE DATABASE cdr_analysis;
    ```
3. Assign all rights to user 

    ```sql
    GRANT ALL PRIVILEGES ON DATABASE cdr_analysis TO postgres;
    ```
4. Exit postgres 

   ```commandline
   exit
   ```

##### Django
1. Create a python 3 virtual environment 

    ```commandline
    virtualenv -p python3 venv
    ```

2. Activate the virtual environment 

    ```commandline
    source venv/bin/activate
    ```

3. Install the project's dependencies 

    ```commandline
    pip install -r requirements.txt
    ```
4. Run the database migrations 

    ```commandline
    python manage.py migrate
    ```
5. Copy the default CDR schema 

    ```commandline
    python manage.py loaddata cdr_schema.json
    ```
6. Copy the default BTS schema 

    ```commandline
    python manage.py loaddata bts_schema.json
    ```
7. Create a superuser 

    ```commandline
    python manage.py createsuperuser
    ```

Running the project
--------------------
##### Development
1. Install front-end dependencies 

    ```commandline
    npm install
    ```
2. Run the code watcher 

    ```commandline
    npm run watch
    ```
3. Start the django development server 

    ```commandline
    python manage.py runserver
    ```

##### Production
> Follow the guide at 
    [DEPLOY DJANGO APP: NGINX, GUNICORN, POSTGRESQL & SUPERVISOR](https://codingstartups.com/deploy-django-nginx-gunicorn-postgresql-supervisor/)