from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from rest_framework.authtoken.models import Token
from django.contrib.auth.views import logout


def get_user_id(request):
    user_id = str(request.user.id)
    return user_id


def index(request):
    user_id = get_user_id(request)
    if user_id is None:
        return render(request, 'accounts/login.html', {})
    else:
        return redirect('/home/', request)


def logout_user(request):
    logout(request)
    return render(request, 'accounts/login.html', {})


def login_form(request):
    return render(request, 'accounts/login.html', {})


def get_auth_token(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        # the password verified for the user
        if user.is_active:
            token, created = Token.objects.get_or_create(user=user)
            request.session['auth'] = token.key
            return redirect('/home/', request)
    return redirect(settings.LOGIN_URL, request)