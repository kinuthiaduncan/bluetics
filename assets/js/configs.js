window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

window.Vue = require('vue');

import VueResource from 'vue-resource';
Vue.use(VueResource);
Vue.http.headers.common['X-CSRF-TOKEN'] = window.Django.csrfToken;

window.Event = new Vue();
require('vue-events');


window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRFToken': window.Django.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
Vue.use(ElementUI, { locale });

import 'es6-promise/auto'
