require('./configs');
import 'vue-cytoscape/dist/vue-cytoscape.css';
import store from './stores/store';
import VueCytoscape from 'vue-cytoscape';
import 'vue-cytoscape/dist/vue-cytoscape.css';
import VueRouter from 'vue-router';
import DataTables from 'vue-data-tables';
import moment from 'moment';
import vuescroll from 'vuescroll';
import 'vuescroll/dist/vuescroll.css';
import { mapActions } from 'vuex';

Vue.use(VueRouter);
Vue.use(VueCytoscape);
Vue.use(DataTables);
Vue.use(vuescroll);
Vue.prototype.moment = moment;

let bus = new Vue();

Vue.component('search', require('./components/Search.vue').default);
Vue.component('frequent_calls', require('./components/Calls.vue').default);
Vue.component('callDetails', require('./components/CallDetails.vue').default);
Vue.component('smsDetails', require('./components/SMSDetails.vue').default);
Vue.component('centrality', require('./components/Centrality.vue').default);
Vue.component('chart', require('./components/Chart.vue').default);
// Vue.component('locations', require('./components/Locations.vue').default);
// Vue.component('location_filter', require('./components/Filters/Locations.vue').default);
Vue.component('heatmap', require('./components/Heatmap.vue').default);
Vue.component('heatmap-details', require('./components/HeatmapDetails.vue').default);
Vue.component('frequency_filter', require('./components/Filters/Frequency.vue').default);
Vue.component('phones_filter', require('./components/Filters/Phones.vue').default);
Vue.component('synspy', require('./components/Synspy.vue').default);
import graph from './components/Graph.vue';
import map from './components/Map.vue';
import upload from './components/Upload.vue';
import cdrs from './components/CDRs.vue';
import settings from './components/Settings.vue';
import sms from './components/SMS.vue';

export const stats = new Vue();
export const details = new Vue();

const routes = [
    {path: '/', component: graph, name: 'graph', alias: '/graph'},
    {path: '/map', component: map, name: 'map'},
    {path: '/upload', component: upload, name: 'upload'},
    {path: '/settings', component: settings, name: 'settings'},
    {path: '/cdrs', component: cdrs, name: 'cdrs'},
    {path: '/sms', component: sms, name: 'sms'},
];

const router = new VueRouter({ routes });


const app = new Vue({
   router,
    store,
    data: function () {
        return {
            recommender_url: '/cdr/recommend/',
            isCallDetailActive: true,
            isSMSDetailActive: false,
            detailSeen:'detailSeen',
            detailInactive:'detailInactive',
            isCentralityActive: false,
            loading: false,
            phone_number: '',
            filterParam: {
                item: '',
                value: ''
            },
            filterOptions: [
                {label: 'Phone number', value: 'phone_number'},
                {label: 'CDR', value: 'cdr'},
                {label: 'Date', value: 'date'},
                {label: 'I.M.E.I.', value: 'imei'},
                {label: 'I.M.S.I.', value: 'imsi'},
                {label: 'Location', value: 'location'},
            ],
            locationFilter: {
                latitude: '',
                longitude: '',
                radius: ''
            },
            filterResults: [],
            workspace_number: '',
            clicked_number: ''
        }
    },
    mounted() {
        Event.$on('callDetails', () => {
             this.isCallDetailActive = true;
             this.isSMSDetailActive = false;
             this.isCentralityActive = false;
        });
        Event.$on('smsDetails', () => {
             this.isSMSDetailActive = true;
             this.isCallDetailActive = false;
             this.isCentralityActive = false;
        });
        this.$store.dispatch('getActiveTheme');
    },
    watch: {
        'filterParam.item': function() {
            this.filterParam.value = '';
            this.filterResults = [];
        }
    },
    computed: {
        active_theme() {
            return this.$store.getters.theme;
        },
        search_toggle() {
            return this.$store.getters.searchBar;
        },
        detail_toggle() {
            return this.$store.getters.detailSection;
        },
        centrality_toggle() {
            return this.$store.getters.centralitySection;
        },
        synspy_toggle() {
            return this.$store.getters.synspySection;
        },
        stats_toggle() {
            return this.$store.getters.statsSection;
        },
        sms_toggle() {
            return this.$store.getters.smsBar;
        },
        right_bar(){
            return this.$store.getters.rightBar;
        },
        map_filters(){
            return this.$store.getters.mapFilters;
        },
        show_heatmap_details(){
            return this.$store.getters.show_heatmap_details;
        }
    },
     methods: {
       ...mapActions([
           'listenToThemeChange',
           'listenToSearchToggle',
           'listenToDetailToggle',
           'listenToStatsToggle',
           'listenToRightBarToggle',
           'heatmapDetails'
       ]),

         searchBarToggle: function() {
           let new_status = !this.$store.getters.searchBar;
           this.listenToSearchToggle(new_status);
         },

         heatmapModalToggle: function(){
           let new_status = !this.$store.getters.show_heatmap_details;
           this.heatmapDetails(new_status);
         },

         rightBarToggle: function() {
           let new_status = !this.$store.getters.rightBar;
           this.listenToRightBarToggle(new_status);
         },

          statsBarToggle: function(event) {
             let new_status = !this.$store.getters.statsSection;
             this.listenToStatsToggle(new_status);
         },

         search: function() {
           if(this.filterParam.item === "location") {
               this.filterParam.value = this.locationFilter;
           }
             Event.$emit('filtering', {
                 phone_number: this.phone_number,
                 filterParam: this.filterParam
             });
         },
         analyze: function() {
             Event.$emit('analyze');
             this.isCallDetailActive = false;
             this.isSMSDetailActive = false;
             this.isCentralityActive = true;
         },

         recommend: function(query) {
            if (query !== '') {
                this.loading = true;

                axios.post(this.recommender_url,  {
                    input: {'item': this.filterParam.item, 'query': query}
                }).then(response => {
                    this.loading = false;
                    if (response.data.success) {
                        this.filterResults = response.data.result;
                    }
                });
            } else {
                this.filterResults = [];
            }
         }
     },

}).$mount('#app');
