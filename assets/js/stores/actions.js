export const  listenToThemeChange = ({commit}, theme) => {
		commit('UPDATE_THEME', theme);
};

export const  listenToSearchToggle = ({commit}, status) => {
    commit('UPDATE_SEARCH', status);
};

export const  listenToStatsToggle = ({commit}, status) => {
    commit('UPDATE_STATS', status);
};

export const listenToMapFiltersToggle = ({commit}, status) => {
    commit('UPDATE_MAP_FILTERS', status);
};

export const listenToNodeChanges = ({commit}, nodes) => {
  commit('UPDATE_NODES', nodes);
};

export const listenToRightBarToggle = ({commit}, status) => {
    commit('UPDATE_RIGHT_BAR', status)
};

export const  listenToDetailToggle = ({commit}, status) => {
    commit('UPDATE_DETAIL', status);
};

export const listenToSMSToggle = ({commit}, status) => {
    commit('UPDATE_SMS_BAR', status);
};

export const  centralityBarControl = ({commit}, status) => {
    commit('UPDATE_CENTRALITY', status);
};

export const  synspyBarControl = ({commit}, status) => {
    commit('UPDATE_SYNSPY', status);
};

export const updateTarget = ({commit}, target) => {
    commit('UPDATE_TARGET', target);
};
export const heatmapDetails = ({commit}, status) => {
    commit('UPDATE_HEATMAP_DETAILS', status);
};
export const heatmapDataUpdate = ({commit}, heatmap_data) => {
    commit('UPDATE_HEATMAP_DATA', heatmap_data);
};