const state = {
    heatmap_data:{
        data: [],
        date: null
    },
};

const mutations = {
    'UPDATE_HEATMAP_DATA'(state, heatmap_data) {
        state.heatmap_data = heatmap_data;
    },
};

const getters = {
    heatmap_data (state) {
        return state.heatmap_data;
    },
};

export default {
	state,
	mutations,
	getters
}