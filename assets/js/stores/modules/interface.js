const state = {
    statsSection: true,
    searchBar: true,
    centralitySection: true,
    detailSection: true,
    rightBar: true,
    smsBar: true,
    synspySection: true,
    mapFilters: false,
    target: null,
	show_heatmap_details: false
};

const mutations = {
	'UPDATE_DETAIL' (state, status) {
		state.detailSection = status;
	},
    'UPDATE_STATS' (state, status) {
		state.statsSection = status;
	},
    'UPDATE_SEARCH' (state, status) {
		state.searchBar = status;
	},
    'UPDATE_CENTRALITY' (state, status) {
		state.centralitySection = status;
	},
	'UPDATE_SYNSPY' (state, status) {
		state.synspySection = status;
	},
	'UPDATE_RIGHT_BAR' (state, status){
	    state.rightBar = status;
    },
    'UPDATE_SMS_BAR' (state, status){
	    state.smsBar = status;
    },
    'UPDATE_MAP_FILTERS' (state, status){
	    state.mapFilters = status;
    },
    'UPDATE_TARGET' (state, target){
	    state.target = target;
    },
    'UPDATE_HEATMAP_DETAILS' (state, status){
	    state.show_heatmap_details = status;
    }
};

const getters = {
	detailSection: (state) => {
		return state.detailSection;
	},
    statsSection: (state) => {
		return state.statsSection;
	},
    searchBar: (state) => {
		return state.searchBar;
	},
    centralitySection: (state) => {
		return state.centralitySection;
	},
    rightBar: (state) => {
	    return state.rightBar;
    },
    smsBar: (state) => {
	    return state.smsBar;
    },
    mapFilters: (state) => {
	    return state.mapFilters;
    },
    synspySection: (state) => {
	    return state.synspySection;
    },
    target: (state) => {
	    return state.target;
    },
    show_heatmap_details: (state) => {
	    return state.show_heatmap_details;
    }
};

export default {
	state,
	mutations,
	getters
}