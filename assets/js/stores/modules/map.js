const state = {
	location_data: {},
    workspace_nodes: {}
};

const mutations = {
    'UPDATE_LOCATIONS' (state, location_data) {
        state.location_data = location_data;
    },

    'UPDATE_NODES' (state, workspace_nodes) {
        state.workspace_nodes = workspace_nodes;
    }
};

const actions = {

};

const getters = {
    location_data: (state) => {
        return state.location_data;
    },
     workspace_nodes: (state) => {
        return state.workspace_nodes;
    }
};

export default {
	state,
	mutations,
	actions,
	getters
}