import axios from "axios";
const state = {
	device: {},
    loading: null
};

const mutations = {
	'UPDATE_DEVICE' (state, device) {
		state.device = device;
	}
};

const actions = {
    getDeviceDetails({commit}, phone_number) {
        state.loading = true;
        const config = {
            email: 'blutics@gmail.com',
            password: 'blutics',
            synspyURL: 'https://digitalspace.xyz/search/v1'
        };
        let accessToken = null;
        let device = null;
        let data = JSON.stringify({
            email: config.email,
            password: config.password
        });
        axios.post(config.synspyURL + '/auth/token', data, {
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            }
        })
            .then(response => {
                accessToken = response.data.accessToken;
                if (accessToken) {
                    axios.get(config.synspyURL +'/search/?q=' + '0' + phone_number, {
                        headers: {
                            'Authorization': 'Bearer ' + accessToken,
                        }
                    })
                        .then(response => {
                            device = response.data.target;
                            commit('UPDATE_DEVICE', device);
                            state.loading = false;
                        });
                }
            }).catch(error => {
            console.log("Error is :", error);
            state.loading = false;
        });
    },
};

const getters = {
	device: (state) => {
		return state.device;
	},
	loading: (state) => {
	    return state.loading;
    }
};

export default {
	state,
	mutations,
	actions,
	getters
}
