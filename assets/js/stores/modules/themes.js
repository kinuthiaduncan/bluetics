const state = {
	theme: {}
};

const mutations = {
	'UPDATE_THEME' (state, theme) {
		state.theme = theme;
	}
};

const actions = {
    getActiveTheme({commit}){
        axios
        .get('/settings/theme')
        .then(response => response.data.data[0])
        .then(theme => {
        commit('UPDATE_THEME', theme)
        });
    }
};

const getters = {
	theme: (state) => {
		return state.theme;
	}
};

export default {
	state,
	mutations,
	actions,
	getters
}