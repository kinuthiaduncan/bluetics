import Vue from 'vue';
import Vuex from 'vuex';

import themes from './modules/themes';
import interfaces from './modules/interface'
import * as actions from './actions';
import map from './modules/map';
import synspy from './modules/synspy';
import heatmap from './modules/heatmap';

Vue.use(Vuex);

export default new Vuex.Store({
	actions,
	modules: {
		themes,
		interfaces,
		map,
        synspy,
		heatmap
	}
});