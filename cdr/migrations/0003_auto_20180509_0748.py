# Generated by Django 2.0.2 on 2018-05-09 07:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cdr', '0002_workspace_workspace_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workspace',
            name='workspace_name',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
