from django.db import models


class CdrData(models.Model):
    callermsisdn = models.CharField(max_length=255, blank=True, null=True)
    calleemsisdn = models.CharField(max_length=255, blank=True, null=True)
    datetime = models.CharField(max_length=255, blank=True, null=True)
    duration = models.CharField(max_length=255, blank=True, null=True)
    callerimei = models.CharField(max_length=255, blank=True, null=True)
    calleeimei = models.CharField(max_length=255, blank=True, null=True)
    callerimsi = models.CharField(max_length=255, blank=True, null=True)
    calleeimsi = models.CharField(max_length=255, blank=True, null=True)
    callerlongitude = models.CharField(max_length=255, blank=True, null=True)
    callerlatitude = models.CharField(max_length=255, blank=True, null=True)
    calleelongitude = models.CharField(max_length=255, blank=True, null=True)
    calleelatitude = models.CharField(max_length=255, blank=True, null=True)
    callerbasestationposition = models.CharField(max_length=255, blank=True, null=True)
    callerbasestationid = models.CharField(max_length=255, blank=True, null=True)
    calleebasestationposition = models.CharField(max_length=255, blank=True, null=True)
    calleebasestationid = models.CharField(max_length=255, blank=True, null=True)
    endlocation = models.CharField(max_length=255, blank=True, null=True)
    subloc = models.CharField(max_length=255, blank=True, null=True)
    sublocname = models.CharField(max_length=255, blank=True, null=True)
    recordtype = models.CharField(max_length=255, blank=True, null=True)
    servicetype = models.CharField(max_length=255, blank=True, null=True)
    datasource = models.CharField(max_length=255, blank=True, null=True)
    roamingvpmn = models.CharField(max_length=255, blank=True, null=True)
    callerlookup = models.CharField(max_length=255, blank=True, null=True)
    calleelookup = models.CharField(max_length=255, blank=True, null=True)
    model = models.CharField(max_length=255, blank=True, null=True)
    calleeoperatorname = models.CharField(max_length=255, blank=True, null=True)
    calleroperatorname = models.CharField(max_length=255, blank=True, null=True)
    cdr_name = models.CharField(max_length=255, blank=True, null=True, default='')
    uploaded_by = models.IntegerField(null=False, default=0)
    uploaded_date = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        verbose_name = "CDR Data record"
        verbose_name_plural = "CDR Data records"

    def __str__(self):
        return self.callermsisdn + " -- " + self.calleemsisdn


class BTSMapper(models.Model):
    radio = models.CharField(max_length=255, blank=True, null=True)
    mcc = models.CharField(max_length=255, blank=True, null=True)
    net = models.CharField(max_length=255, blank=True, null=True)
    area = models.CharField(max_length=255, blank=True, null=True)
    cell = models.CharField(max_length=255, blank=True, null=True)
    unit = models.CharField(max_length=255, blank=True, null=True)
    lon = models.CharField(max_length=255, blank=True, null=True)
    lat = models.CharField(max_length=255, blank=True, null=True)
    range = models.CharField(max_length=255, blank=True, null=True)
    samples = models.CharField(max_length=255, blank=True, null=True)
    changeable = models.CharField(max_length=255, blank=True, null=True)
    created = models.CharField(max_length=255, blank=True, null=True)
    updated = models.CharField(max_length=255, blank=True, null=True)
    averageSignal = models.CharField(max_length=255, blank=True, null=True)

