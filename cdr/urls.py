"""cdr URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from rest_framework_jwt.views import refresh_jwt_token
from accounts import views
from . import views as local_views


admin.site.site_header = 'Bluetics Adminstration'
admin.site.site_title = 'Bluetics Adminstration'
admin.site.index_title = 'Bluetics Adminstration'

urlpatterns = [
    path('', views.index),
    path('admin/', admin.site.urls),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^refresh-token/', refresh_jwt_token),
    url('cdr/upload/', local_views.UploadCdr.as_view()),
    url('cdr/recommend', local_views.Recommend.as_view()),
    url('cdr/rename/node', local_views.RenameNode.as_view()),
    url('cdr/mine/', local_views.MyCDRs.as_view()),
    url('cdr/hour/details', local_views.Heatmap.as_view()),
    path('accounts/', include('accounts.urls', namespace='accounts')),
    path('home/', include('home.urls', namespace='home')),
    path('workspace/', include('workspace.urls')),
    path('schema/', include('schema.urls')),
    path('search/', include('search.urls')),
    path('graph/', include('graph.urls')),
    path('settings/', include('settings.urls')),
    path('map/', include('map.urls')),
]
