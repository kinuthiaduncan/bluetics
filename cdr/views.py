from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser

from django.db import connection
from django.db.models import Q
from .models import CdrData, BTSMapper
from schema.models import Schema

import os.path
import pandas as pd
import phonenumbers

import datetime


class UploadCdr(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        file_name = request.data['file_name']
        file_name = file_name.replace('"', '')
        file_obj = request.FILES['file']
        file_status = self.inspect_file(file_obj)
        df = file_status["data_frame"]
        outliers = file_status["outliers"]
        headers = file_status["schema"]
        if not outliers:
            clean_df = self.clean_data_frame(df)
            save = self.save_dataframe(clean_df, file_name, request.user.id)
            return Response({"success": save})
        return Response({"success": False, "outliers": outliers, "schema": headers})

    def inspect_file(self, file_obj):
        file_name = file_obj.name
        file_type = self.get_file_type(file_name)

        if file_type == "csv":
            file_obj = self.csv_to_data_frame(file_obj)
            if file_obj is None:
                return None
            file_status = self.inspect_data_frame(file_obj)
        elif file_type == "htm" or file_type == "html":
            file_obj = self.html_to_data_frame(file_obj)
            if file_obj is None:
                return None
            file_status = self.inspect_data_frame(file_obj)
        elif file_type == "xls":
            file_obj = self.spreadsheet_to_data_frame(file_obj)
            if file_obj is None:
                return None
            file_status = self.inspect_data_frame(file_obj)
        else:
            return None

        return file_status

    @staticmethod
    def get_file_type(file_name):
        extension = os.path.splitext(file_name)[1][1:]
        return extension

    @staticmethod
    def csv_to_data_frame(file_obj):
        data_frame = pd.read_csv(file_obj, dtype=str)
        return data_frame

    @staticmethod
    def html_to_data_frame(file_obj):
        file_obj = file_obj.read()
        data_frame = pd.read_html(file_obj, header=0)[0]
        return data_frame

    @staticmethod
    def spreadsheet_to_data_frame(file_obj):
        frames = []
        xls_file = pd.ExcelFile(file_obj)
        sheets = xls_file.sheet_names
        for i in sheets:
            df = xls_file.parse(i)
            frames.append(df)
        data_frame = pd.concat(frames, ignore_index=True)
        return data_frame

    @staticmethod
    def inspect_data_frame(df):
        # fetch existing headers from db
        schema = Schema.objects.latest('id')
        existing_headers = schema.content

        df = df.loc[:, ~df.columns.str.contains('Unnamed')]
        original_columns = df.columns
        outliers = []
        for header in original_columns:
            if header.lower() in existing_headers.keys():
                df.rename(columns={header: existing_headers[header.lower()]}, inplace=True)
            else:
                outliers.append(header)
        schema_final = []
        schema_details = {}
        for header in list(set(existing_headers.values())):
            try:
                schema_details['name'] = header
                schema_final.append(schema_details)
            except:
                pass
            finally:
                schema_details = {}

        result = {
            "data_frame": df,
            "outliers": outliers,
            "schema": existing_headers
        }

        return result

    @staticmethod
    def clean_phone_number(phone_number):
        result = ""
        try:
            parse_result = phonenumbers.parse(phone_number, None)
            result = phonenumbers.format_number(parse_result, phonenumbers.PhoneNumberFormat.E164)
        except phonenumbers.phonenumberutil.NumberParseException as e:
            if e.error_type == 0:
                parse_result = phonenumbers.parse(phone_number, "KE")
                result = phonenumbers.format_number(parse_result, phonenumbers.PhoneNumberFormat.E164)
        finally:
            return result

    @staticmethod
    def clean_coordinate(coordinate):
        if coordinate == 'nan':
            return None

    def clean_data_frame(self, df):
        new_headers = df.columns

        coordinates_helper = {
            'basestationid': {'lat': 'callerlatitude', 'lon': 'callerlongitude'},
            'callerbasestationid': {'lat': 'callerlatitude', 'lon': 'callerlongitude'},
            'calleebasestationid': {'lat': 'calleelatitude', 'lon': 'calleelongitude'}
        }

        # convert duration to seconds
        if "duration" in new_headers:
            df["duration"] = pd.to_timedelta(df["duration"]).dt.total_seconds()

        if "datetime" not in new_headers:
            if "time" in new_headers:
                df.rename(columns={"time": "datetime"}, inplace=True)
                df["datetime"] = pd.to_datetime(df["datetime"])
        else:
            if "time" in new_headers:
                df.drop(["time"], axis=1, inplace=True)
            df["datetime"] = pd.to_datetime(df["datetime"])

        # fix records that have a call direction
        for i in new_headers:
            if i == "calltype":
                direction = ((df[i] == "Incoming") | (df[i] == "Called"))
                # swap msisdn
                df.loc[direction] = df.loc[direction].rename(
                    columns={'callermsisdn': 'calleemsisdn', 'calleemsisdn': 'callermsisdn'})
                # swap lookups
                df.loc[direction] = df.loc[direction].rename(
                    columns={'callerlookup': 'calleelookup', 'calleelookup': 'callerlookup'})
                # swap imei
                df.loc[direction] = df.loc[direction].rename(
                    columns={'callerimei': 'calleeimei', 'calleeimei': 'callerimei'})
                df.drop([i], axis=1, inplace=True)
                break

        for header in new_headers:
            if header in ['basestationid', 'callerbasestationid', 'calleebasestationid']:
                stations = df[header]
                helper = coordinates_helper[header]
                for i in stations:
                    station_id = str(i)
                    if station_id is not "NaN":
                        split_id = station_id.split("-")
                        if len(split_id) == 4:
                            mcc = split_id[0]
                            net = split_id[1]
                            area = split_id[2]
                            cell = split_id[3]

                            if BTSMapper.objects.filter(mcc=mcc, net=net, area=area, cell=cell).first():
                                station_data = BTSMapper.objects.filter(mcc=mcc, net=net, area=area, cell=cell).first()
                                df.loc[df[header] == i, [helper['lat'], helper['lon']]] = station_data.lat, station_data.lon
                            else:
                                if net[0] == '0':
                                    net = net[1:]
                                if area[0] == '0':
                                    area = area[1:]
                                if cell[0] == '0':
                                    cell = cell[1:]

                                if BTSMapper.objects.filter(mcc=mcc, net=net, area=area, cell=cell).first():
                                    station_data = BTSMapper.objects.filter(mcc=mcc, net=net, area=area,
                                                                            cell=cell).first()
                                    df.loc[df[header] == i, [helper['lat'], helper['lon']]] = station_data.lat, station_data.lon

                        elif station_id[:3] == '639':
                            mcc = str(639)
                            net = station_id[3:5]
                            area = station_id[5:10]
                            if len(station_id) == 15:
                                cell = station_id[10:15]

                                if BTSMapper.objects.filter(mcc=mcc, net=net, area=area, cell=cell).first():
                                    station_data = BTSMapper.objects.filter(mcc=mcc, net=net, area=area,
                                                                            cell=cell).first()
                                    df.loc[df[header] == i, [helper['lat'], helper['lon']]] = station_data.lat, station_data.lon
                                else:
                                    if net[0] == '0':
                                        net = net[1:]
                                    if area[0] == '0':
                                        area = area[1:]
                                    if cell[0] == '0':
                                        cell = cell[1:]

                                    if BTSMapper.objects.filter(mcc=mcc, net=net, area=area, cell=cell).first():
                                        station_data = BTSMapper.objects.filter(mcc=mcc, net=net, area=area,
                                                                                cell=cell).first()

                                        df.loc[df[header] == i, [helper['lat'], helper['lon']]] = station_data.lat, station_data.lon
                            else:
                                if BTSMapper.objects.filter(
                                        Q(mcc=mcc, net=net, area=area) | Q(mcc=mcc, net=net, cell=area)).first():
                                    station_data = BTSMapper.objects.filter(
                                        Q(mcc=mcc, net=net, area=area) | Q(mcc=mcc, net=net, cell=area)).first()
                                    df.loc[df[header] == i, [helper['lat'], helper['lon']]] = station_data.lat, station_data.lon
                                else:
                                    if net[0] == '0':
                                        net = net[1:]
                                    if area[0] == '0':
                                        area = area[1:]
                                    if BTSMapper.objects.filter(
                                            Q(mcc=mcc, net=net, area=area) | Q(mcc=mcc, net=net, cell=area)).first():
                                        station_data = BTSMapper.objects.filter(
                                            Q(mcc=mcc, net=net, area=area) | Q(mcc=mcc, net=net, cell=area)).first()
                                        df.loc[df[header] == i, [helper['lat'], helper['lon']]] = station_data.lat, station_data.lon

        # standardize phone numbers
        df["callermsisdn"] = df["callermsisdn"].apply(self.clean_phone_number)
        df["calleemsisdn"] = df["calleemsisdn"].apply(self.clean_phone_number)

        # standardize coodinates
        for column in ["callerlatitude", "callerlongitude", "calleelatitude", "calleelongitude"]:
            if column in df.columns:
                df[column] = df[column].apply(self.clean_coordinate)

        if 'basestationid' in new_headers:
            df.rename(columns={'basestationid': 'callerbasestationid'}, inplace=True)

        return df

    @staticmethod
    def save_dataframe(df, cdr_name, user_id):
        for item in df.to_dict('records'):
            item['cdr_name'] = cdr_name
            item['uploaded_by'] = user_id
            try:
                CdrData.objects.create(**item)
            except:
                pass

        return True


class Recommend(APIView):
    def __init__(self):
        self.parameter = ''

    def post(self, request):
        result = []
        data = request.data['input']
        item = data['item']
        self.parameter = '%' + data['query'] + '%'

        generic_filter = {
            'phone_number': {'field1': 'callermsisdn', 'field2': 'calleemsisdn'},
            'imei': {'field1': 'callerimei', 'field2': 'calleeimei'},
            'imsi': {'field1': 'callerimsi', 'field2': 'calleeimsi'},
        }

        if item in generic_filter:
            result = self.generic(generic_filter[item])
        elif item == "cdr":
            result = self.cdr()

        return Response({"success": True, "result": result})

    def generic(self, item):
        cursor = connection.cursor()

        cursor.execute("SELECT DISTINCT {} AS name "
                       "FROM cdr_cdrdata "
                       "WHERE {} LIKE '{}' "
                       "UNION "
                       "SELECT {} AS name "
                       "FROM cdr_cdrdata "
                       "WHERE {} LIKE '{}' "
                       "LIMIT 20".format(item['field1'], item['field1'], self.parameter,
                                         item['field2'], item['field2'], self.parameter))
        columns = [column[0] for column in cursor.description]
        suggestions = []
        for row in cursor.fetchall():
            suggestions.append(dict(zip(columns, row)))
        return suggestions

    def cdr(self):
        cursor = connection.cursor()

        cursor.execute("SELECT DISTINCT cdr_name AS name "
                       "FROM cdr_cdrdata "
                       "WHERE cdr_name LIKE '{}'  "
                       "LIMIT 20".format(self.parameter))
        columns = [column[0] for column in cursor.description]
        suggestions = []
        for row in cursor.fetchall():
            suggestions.append(dict(zip(columns, row)))
        return suggestions


class RenameNode(APIView):
    def post(self, request):
        node_id = request.data['old_name']
        new_name = request.data['new_name']

        for cdr in CdrData.objects.filter(callermsisdn=node_id):
            cdr.callerlookup = new_name
            cdr.save()

        for cdr in CdrData.objects.filter(calleemsisdn=node_id):
            cdr.calleelookup = new_name
            cdr.save()

        return Response({"success": True})


class MyCDRs(APIView):
    def get(self, request):
        cdrs = CdrData.objects.filter(uploaded_by=request.user.id).distinct('cdr_name')\
            .values('cdr_name', 'uploaded_date')
        return Response({"success": True, "result": cdrs})


class Heatmap(APIView):
    def get(self, request):
        target = request.query_params['target']
        start = datetime.datetime.strptime(request.query_params['start'], "%Y-%m-%d %H:%M:%S")
        end = datetime.datetime.strptime(request.query_params['end'], "%Y-%m-%d %H:%M:%S")
        NAN_FLAG = 'nan'

        cursor = connection.cursor()
        cursor.execute(
            "SELECT "
            "callermsisdn AS caller, "
            "calleemsisdn AS callee, "
            "callerlookup AS source_name, "
            "calleelookup AS target_name "
            "FROM "
            "cdr_cdrdata "
            "WHERE "
            "callermsisdn = '{}' "
            "AND "
            "calleemsisdn IS NOT NULL "
            "AND "
            "datetime BETWEEN '{}' AND '{}' "
            "OR "
            "calleemsisdn = '{}' "
            "AND "
            "callermsisdn IS NOT NULL "
            "AND "
            "datetime BETWEEN '{}' AND '{}' "
            .format(target, start, end, target, start, end))

        columns = [column[0] for column in cursor.description]
        details = []
        for row in cursor.fetchall():
            details.append(dict(zip(columns, row)))

        return Response({"success": True, "details": details})

