from django.conf.urls import url
from . import views as local_views

urlpatterns = [
    url('node', local_views.NodeView.as_view()),
    url('edge', local_views.EdgeView.as_view()),
]
