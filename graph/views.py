from rest_framework.views import APIView
from rest_framework.response import Response

from django.db import connection


class NodeView(APIView):
    def get(self, request):
        phone_number = request.query_params['phone_number']
        cursor = connection.cursor()
        cursor.execute("SELECT * "
                       "FROM cdr_cdrdata "
                       "WHERE callermsisdn = '{}'".format(phone_number))
        columns = [column[0] for column in cursor.description]
        number_stats = []
        for row in cursor.fetchall():
            number_stats.append(dict(zip(columns, row)))
        return Response({"success": True, "content": number_stats})


class EdgeView(APIView):
    def get(self, request):
        source = request.query_params['source']
        target = request.query_params['target']
        cursor = connection.cursor()
        cursor.execute(
            "SELECT * "
            "FROM cdr_cdrdata "
            "WHERE "
                "callermsisdn = '{}' "
                "AND "
                "calleemsisdn = '{}'".format(source, target))
        columns = [column[0] for column in cursor.description]
        edge_stats = []
        for row in cursor.fetchall():
            edge_stats.append(dict(zip(columns, row)))
        return Response({"success": True, "content": edge_stats})
