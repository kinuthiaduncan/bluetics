#!/bin/bash

NAME="bluetics"                                       # Name of the application
DJANGODIR=/home/bluetics/bluetics                     # Django project directory
SOCKFILE=/home/bluetics/venv/run/gunicorn.sock        # We will communicate using this unix socket
USER=bluetics                                         # The user to run as
GROUP=bluetics                                        # The group to run as
NUM_WORKERS=3                                         # How many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=cdr.settings                   # Which settings file should Django use
DJANGO_WSGI_MODULE=cdr.wsgi                           # WSGI module name
TIMEOUT=120000                                          # Worker process timeout
echo "Starting $NAME as `whoami`"

# Activate the virtual environment

cd $DJANGODIR
source /home/bluetics/venv/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist

RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR
# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)

exec gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --worker-class gevent \
  --timeout $TIMEOUT \
  --user=$USER \
  --bind 0.0.0.0:8000 \
  --log-level=debug \
  --log-file=-