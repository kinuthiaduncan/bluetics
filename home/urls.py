from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views as local_views

app_name = 'home'

urlpatterns = [
    url(r'^$', local_views.index),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)