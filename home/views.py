from django.shortcuts import render, redirect


def check_auth(request):
    user_id = request.user.id
    if user_id is None:
        return False
    return True


def index(request):
    if check_auth(request):
        return render(request, 'home/index.html')
    return render(request, 'accounts/login.html')
