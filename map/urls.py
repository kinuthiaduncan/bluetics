from django.conf.urls import url
from . import views as local_views

urlpatterns = [
    url('locations', local_views.Locations.as_view()),
    ]
