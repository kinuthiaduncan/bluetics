from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db import connection
from cdr.models import CdrData

# Create your views here.


class Locations(APIView):
    def post(self, request):
        nodes = request.data
        cursor = connection.cursor()
        result = []
        if nodes:
            cursor.execute(
                "SELECT "
                "caller, callee, datetime, latitude, longitude "
                "FROM("
                "SELECT "
                "callermsisdn as caller, calleemsisdn as callee, datetime as datetime, callerlatitude as latitude,"
                "callerlongitude as longitude "
                "FROM cdr_cdrdata "
                "WHERE "
                "callermsisdn IN {} "
                "AND "
                "callermsisdn != '{}' "
                "AND "
                "callerlatitude != '{}' "
                "AND "
                "callerlongitude != '{}' "
                "UNION "
                "SELECT "
                "callermsisdn as caller, calleemsisdn as callee, datetime as datetime, calleelatitude as latitude,"
                "calleelongitude as longitude "
                "FROM cdr_cdrdata "
                "WHERE "
                "calleemsisdn IN {} "
                "AND "
                "calleemsisdn != '{}' "
                "AND "
                "calleelatitude != '{}' "
                "AND "
                "calleelongitude != '{}' "
                ") AS a "
                .format(str(nodes).replace('[', '(').replace(']', ')') if len(nodes) == 1 else tuple(nodes), 'nan', 'nan', 'nan',
                        str(nodes).replace('[', '(').replace(']', ')') if len(nodes) == 1 else tuple(nodes), 'nan', 'nan', 'nan'))

            columns = [column[0] for column in cursor.description]
            for row in cursor.fetchall():
                result.append(dict(zip(columns, row)))

        return Response({"success": True, "result": result})
