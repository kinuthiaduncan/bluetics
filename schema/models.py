from django.db import models
from django.contrib.postgres.fields import JSONField


# Create your models here.
class Schema(models.Model):

    class Meta:
        verbose_name = "Schema"
        verbose_name_plural = "Schema"

    content = JSONField()

    def __str__(self):
        return "Schema Mapper"