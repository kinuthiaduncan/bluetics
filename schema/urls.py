from django.conf.urls import url
from . import views as local_views

urlpatterns = [
    url('update', local_views.UpdateView.as_view()),
]
