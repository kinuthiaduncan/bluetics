from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Schema


class UpdateView(APIView):
    def post(self, request):
        schema = Schema.objects.first()
        schema.content = request.data['schema']
        schema.save()

        return Response({"success": True})
