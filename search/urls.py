from django.conf.urls import url
from . import views as local_views

urlpatterns = [
    url('phone', local_views.PhoneNumber.as_view()),
    url('detail', local_views.Detail.as_view()),
    url('imei', local_views.IMEI.as_view()),
]
