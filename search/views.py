from rest_framework.views import APIView
from rest_framework.response import Response

from django.db import connection


def filter_mapper(item):
    filters_map = {
        'phone_number': {'field1': 'calleemsisdn', 'field2': 'callermsisdn', 'rule': ' LIKE', 'wildcard': True},
        'cdr': {'field1': 'cdr_name', 'field2': 'cdr_name', 'rule': ' LIKE', 'wildcard': True},
        'date': {'field1': 'DATE(datetime)', 'field2': 'DATE(datetime)', 'rule': '::daterange @>', 'wildcard': False},
        'imei': {'field1': 'calleeimei', 'field2': 'callerimei', 'rule': ' LIKE', 'wildcard': True},
        'imsi': {'field1': 'calleeimsi', 'field2': 'callerimsi', 'rule': ' LIKE', 'wildcard': True},
        'location': {
            'field1': 'earth_box(ll_to_earth(calleelatitude::double precision, calleelongitude::double precision)',
            'field2': 'earth_box(ll_to_earth(callerlatitude::double precision, callerlongitude::double precision)',
            'rule': ' @>', 'wildcard': False},
    }
    return filters_map[item] or None


class PhoneNumber(APIView):
    def post(self, request):
        phone_number = '%' + request.data['phone_number'] + '%'
        item = request.data['filter_item'] or None
        param = request.data['filter_value'] or None
        result = []
        cursor = connection.cursor()

        if not item or not param:
            cursor.execute(
                "SELECT DISTINCT "
                    "CASE "
                        "WHEN "
                            "callermsisdn LIKE '{}' AND calleemsisdn IS NOT NULL "
                        "THEN callermsisdn "
                        "WHEN "
                            "calleemsisdn LIKE '{}' AND callermsisdn IS NOT NULL "
                        "THEN calleemsisdn "
                        "END "
                    "AS phone_number FROM cdr_cdrdata".format(phone_number, phone_number))
            columns = [column[0] for column in cursor.description]
            for row in cursor.fetchall():
                result.append(dict(zip(columns, row)))

        else:
            fields = filter_mapper(item)
            if fields:
                if fields['wildcard']:
                    param = '%' + param + '%'

                if fields['rule'] == '::daterange @>':
                    temp = fields['field1']
                    fields['field1'] = "'" + str(param).replace("'", "") + "'"
                    fields['field2'] = "'" + str(param).replace("'", "") + "'"
                    param = temp
                elif fields['rule'] == ' @>':
                    if not param['radius']:
                        param['radius'] = '0'
                    if not param['latitude']:
                        param['latitude'] = 'null'
                    if not param['longitude']:
                        param['longitude'] = 'null'
                    fields['field1'] += ' ,' + param['radius'] + ')'
                    fields['field2'] += ' ,' + param['radius'] + ')'
                    param = ' ll_to_earth({}::double precision, {}::double precision)'\
                        .format(param['latitude'], param['longitude'])
                else:
                    param = "'" + param + "'"
                cursor.execute(
                    "SELECT DISTINCT "
                        "CASE "
                            "WHEN "
                                "callermsisdn LIKE '{}' "
                                "AND "
                                "{}{} {} "
                            "THEN "
                                "callermsisdn "
                            "WHEN "
                                "calleemsisdn LIKE '{}' "
                                "AND "
                                "{}{} {} "
                            "THEN "
                                "calleemsisdn "
                            "END "
                        "AS phone_number "
                    "FROM cdr_cdrdata".format(phone_number, fields['field1'], fields['rule'], param, phone_number, fields['field2'], fields['rule'], param))
                columns = [column[0] for column in cursor.description]
                for row in cursor.fetchall():
                    if row[0]:
                        result.append(dict(zip(columns, row)))

        return Response({"success": True, "result": result})


class Detail(APIView):
    def post(self, request):
        phone_number = request.data['phone_number']
        filter_item = request.data['filter_item']
        filter_value = request.data['filter_value']
        related_entities = []
        phones_used = []
        common_locations = []
        graph_data = []
        dates = []
        graph = {}
        NAN_FLAG = 'nan'

        cursor = connection.cursor()

        if not filter_item or not filter_value:
            cursor.execute(
                "SELECT "
                "DISTINCT "
                    "CASE "
                        "WHEN "
                            "callermsisdn = '{}' AND calleemsisdn != '{}' "
                        "THEN "
                            "calleemsisdn "
                        "WHEN "
                            "calleemsisdn = '{}' AND callermsisdn != '{}' "
                        "THEN "
                            "callermsisdn "
                        "END "
                    "AS phone_number, "
                    "COUNT(*) AS frequency "
                "FROM cdr_cdrdata "
                "GROUP BY phone_number "
                "ORDER BY frequency DESC".format(phone_number, NAN_FLAG, phone_number, NAN_FLAG))
            columns = [column[0] for column in cursor.description]
            for row in cursor.fetchall():
                if row[0]:
                    related_entities.append(dict(zip(columns, row)))

            cursor.execute("SELECT "
                "DISTINCT "
                    "CASE "
                        "WHEN "
                            "callermsisdn = '{}' AND callerimei != '{}' AND callerimei IS NOT NULL "
                        "THEN callerimei "
                        "WHEN "
                            "calleemsisdn = '{}' AND calleeimei != '{}' AND calleeimei IS NOT NULL "
                        "THEN calleeimei "
                        "END "
                    "AS imei, "
                    "COUNT(*) AS frequency "
                "FROM cdr_cdrdata "
                "GROUP BY imei "
                "ORDER BY frequency DESC".format(phone_number, NAN_FLAG, phone_number, NAN_FLAG))

            columns = [column[0] for column in cursor.description]
            for row in cursor.fetchall():
                if row[0]:
                    phones_used.append(dict(zip(columns, row)))

            cursor.execute("SELECT "
                "DISTINCT "
                    "CASE "
                        "WHEN "
                            "callermsisdn = '{}' AND datetime != '{}' AND datetime IS NOT NULL "
                        "THEN datetime "
                        "WHEN "
                            "calleemsisdn = '{}' AND datetime != '{}' AND datetime IS NOT NULL "
                        "THEN datetime "
                        "END "
                    "AS datetime, "
                  "COUNT(*) AS frequency "  
                "FROM cdr_cdrdata "
                "GROUP BY datetime, callermsisdn, calleemsisdn ".format(phone_number, NAN_FLAG, phone_number, NAN_FLAG))

            columns = [column[0] for column in cursor.description]
            for row in cursor.fetchall():
                if row[0]:
                    dates.append(dict(zip(columns, row)))


            cursor.execute(
                "SELECT DISTINCT "
                    "CASE "
                        "WHEN "
                            "callermsisdn = '{}' AND callerlatitude != '{}' AND callerlongitude != '{}' "
                        "THEN CONCAT(callerlatitude, ', ', callerlongitude) "
                        "WHEN "
                            "calleemsisdn = '{}' AND calleelatitude != '{}' AND calleelongitude != '{}' "
                        "THEN CONCAT(calleelatitude, ', ', calleelongitude) "
                        "END "
                    "AS location, "
                    "COUNT(*) AS frequency "
                    "FROM cdr_cdrdata "
                    "GROUP BY location "
                    "ORDER BY frequency DESC".format(phone_number, NAN_FLAG, NAN_FLAG, phone_number, NAN_FLAG, NAN_FLAG))

            columns = [column[0] for column in cursor.description]
            for row in cursor.fetchall():
                if row[0]:
                    common_locations.append(dict(zip(columns, row)))

            cursor.execute(
                "SELECT "
                    "id, "
                    "callermsisdn AS source, "
                    "calleemsisdn AS target, "
                    "datetime, "
                    "callerlookup AS source_name, "
                    "calleelookup AS target_name "
                "FROM "
                    "cdr_cdrdata "
                "WHERE "
                    "callermsisdn = '{}' "
                    "AND "
                    "calleemsisdn IS NOT NULL "
                    "OR "
                    "calleemsisdn = '{}' "
                    "AND "
                    "callermsisdn IS NOT NULL".format(phone_number, phone_number))
            graph_data = cursor.fetchall()
        else:
            fields = filter_mapper(filter_item)
            if fields:
                if fields['wildcard']:
                    filter_value = '%' + filter_value + '%'
                if fields['rule'] == '::daterange @>':
                    temp = fields['field1']

                    fields['field1'] = "'[" + str(filter_value) + "]'"
                    fields['field2'] = "'[" + str(filter_value) + "]'"
                    filter_value = temp
                elif fields['rule'] == ' @>':
                    if not filter_value['radius']:
                        filter_value['radius'] = '0'
                    if not filter_value['latitude']:
                        filter_value['latitude'] = 'null'
                    if not filter_value['longitude']:
                        filter_value['longitude'] = 'null'
                    fields['field1'] += ' ,' + filter_value['radius'] + ')'
                    fields['field2'] += ' ,' + filter_value['radius'] + ')'
                    filter_value = ' ll_to_earth({}::double precision, {}::double precision)'\
                        .format(filter_value['latitude'], filter_value['longitude'])
                else:
                    filter_value = "'" + filter_value + "'"

                cursor.execute(
                    "SELECT DISTINCT "
                        "CASE "
                        "WHEN "
                            "callermsisdn = '{}' "
                            "AND "
                            "{}{} {} "
                            "AND calleemsisdn != '{}' "
                        "THEN "
                            "calleemsisdn "
                        "WHEN "
                            "calleemsisdn = '{}' "
                            "AND "
                            "{}{} {} "
                            "AND callermsisdn != '{}' "
                        "THEN "
                            "callermsisdn "
                        "END "
                        "AS phone_number, "
                        "COUNT(*) AS frequency "
                    "FROM cdr_cdrdata "
                    "GROUP BY phone_number "
                    "ORDER BY frequency DESC".format(phone_number, fields['field1'], fields['rule'], filter_value, NAN_FLAG, phone_number, fields['field2'], fields['rule'], filter_value, NAN_FLAG))
                columns = [column[0] for column in cursor.description]
                for row in cursor.fetchall():
                    if row[0]:
                        related_entities.append(dict(zip(columns, row)))

                cursor.execute(
                    "SELECT DISTINCT "
                    "CASE "
                        "WHEN "
                            "callermsisdn = '{}' AND {}{} {} AND callerimei != '{}' AND callerimei IS NOT NULL "
                        "THEN callerimei "
                        "WHEN "
                            "calleemsisdn = '{}' AND {}{} {} AND calleeimei != '{}' AND calleeimei IS NOT NULL "
                        "THEN calleeimei "
                        "END "
                    "AS imei, "
                    "COUNT(*) AS frequency "
                    "FROM cdr_cdrdata "
                    "GROUP BY imei "
                    "ORDER BY frequency DESC".format(phone_number, fields['field1'], fields['rule'], filter_value, NAN_FLAG, phone_number, fields['field2'], fields['rule'], filter_value, NAN_FLAG))

                columns = [column[0] for column in cursor.description]
                for row in cursor.fetchall():
                    if row[0]:
                        phones_used.append(dict(zip(columns, row)))

                cursor.execute(
                    "SELECT DISTINCT "
                    "CASE "
                        "WHEN "
                            "callermsisdn = '{}' AND {}{} {} AND datetime != '{}' AND datetime IS NOT NULL "
                        "THEN datetime "
                        "WHEN "
                            "calleemsisdn = '{}' AND {}{} {} AND datetime != '{}' AND datetime IS NOT NULL "
                        "THEN datetime "
                        "END "
                    "AS date, "
                    "COUNT(*) AS frequency "
                    "FROM cdr_cdrdata "
                    "GROUP BY date "
                    "ORDER BY frequency DESC".format(phone_number, fields['field1'], fields['rule'], filter_value, NAN_FLAG, phone_number, fields['field2'], fields['rule'], filter_value, NAN_FLAG))

                columns = [column[0] for column in cursor.description]
                for row in cursor.fetchall():
                    if row[0]:
                        dates.append(dict(zip(columns, row)))

                cursor.execute(
                    "SELECT DISTINCT "
                        "CASE "
                            "WHEN "
                                "callermsisdn = '{}' AND {}{} {} AND callerlatitude != '{}' AND callerlongitude != '{}' "
                            "THEN CONCAT(callerlatitude, ', ', callerlongitude) "
                            "WHEN "
                                "calleemsisdn = '{}' AND {}{} {} AND calleelatitude != '{}' AND calleelongitude != '{}' "
                            "THEN CONCAT(calleelatitude, ', ', calleelongitude) "
                            "END "
                        "AS location, "
                        "COUNT(*) AS frequency "
                        "FROM cdr_cdrdata "
                        "GROUP BY location "
                        "ORDER BY frequency DESC".format(phone_number, fields['field1'], fields['rule'], filter_value, NAN_FLAG, NAN_FLAG, phone_number, fields['field2'], fields['rule'], filter_value, NAN_FLAG, NAN_FLAG))

                columns = [column[0] for column in cursor.description]
                for row in cursor.fetchall():
                    if row[0]:
                        common_locations.append(dict(zip(columns, row)))

                cursor.execute(
                    "SELECT "
                        "id, "
                        "callermsisdn AS source, "
                        "calleemsisdn AS target, "
                        "datetime, "
                        "callerlookup AS source_name, "
                        "calleelookup AS target_name "
                    "FROM "
                        "cdr_cdrdata "
                    "WHERE "
                        "callermsisdn = '{}' "
                        "AND "
                        "calleemsisdn IS NOT NULL "
                        "AND "
                        "{}{} {} "
                        "OR "
                        "calleemsisdn = '{}' "
                        "AND "
                        "callermsisdn IS NOT NULL "
                        "AND "
                        "{}{} {} ".format(phone_number, fields['field1'], fields['rule'], filter_value, phone_number, fields['field2'], fields['rule'], filter_value))
                graph_data = cursor.fetchall()

        for row in graph_data:
            call_key = str(row[1]) + str(row[2])
            if call_key in graph.keys():
                new_frequency = graph[call_key]["frequency"] + 1
                graph[call_key] = {"id": row[0], "source": row[1], "target": row[2], "frequency": new_frequency,
                                   "datetime": row[3], "source_name": row[4], "target_name": row[5]}
            else:
                graph[call_key] = {"id": row[0], "source": row[1], "target": row[2], "frequency": 1,
                                   "datetime": row[3], "source_name": row[4], "target_name": row[5]}

        context = {
            'graph_data': graph,
            'related_entities': related_entities,
            'phones_used': phones_used,
            'common_locations': common_locations,
            'dates': dates
        }
        return Response({"success": True, "content": context})


class IMEI(APIView):
    def get(self, request):
        cursor = connection.cursor()
        imei = request.query_params['imei']
        result = []
        cursor.execute(
            "SELECT DISTINCT "
            "CASE "
                "WHEN "
                    "callerimei = '{}' "
                "THEN callermsisdn "
                "WHEN "
                    "calleeimei = '{}' "
                "THEN calleemsisdn "
            "END "
            "AS phone_number, "
            "COUNT(*) AS frequency "
            "FROM cdr_cdrdata "
            "GROUP BY phone_number "
            "ORDER BY frequency DESC".format(imei, imei))

        columns = [column[0] for column in cursor.description]
        for row in cursor.fetchall():
            if row[0]:
                result.append(dict(zip(columns, row)))

        return Response({"success": True, "result": result})
