# Generated by Django 2.0.2 on 2018-09-18 06:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('settings', '0004_themes_image'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Themes',
            new_name='Theme',
        ),
    ]
