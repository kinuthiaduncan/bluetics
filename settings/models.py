from django.db import models

# Create your models here.


class Theme(models.Model):
    name = models.CharField(max_length=255, blank=True, null=False)
    slug = models.CharField(max_length=255, blank=False, null=False)
    workspace_background = models.CharField(max_length=255, blank=True, null=False)
    sidebars_background = models.CharField(max_length=255, blank=True, null=False)
    accent_color = models.CharField(max_length=255, blank=True, null=False)
    node_color = models.CharField(max_length=255, blank=True, null=False)
    node_text_color = models.CharField(max_length=255, blank=True, null=False)
    node_highlight_color = models.CharField(max_length=255, blank=True, null=False)
    node_highlight_text_color = models.CharField(max_length=255, blank=True, null=False)
    edge_color = models.CharField(max_length=255, blank=True, null=False)
    edge_text_color = models.CharField(max_length=255, blank=True, null=False)
    edge_color_range = models.CharField(max_length=255, blank=True, null=False)
    text_color = models.CharField(max_length=255, blank=True, null=False)
    image = models.CharField(max_length=1000, blank=True, null=True)


class UserTheme(models.Model):
    user = models.IntegerField(default=0, null=False)
    theme = models.CharField(max_length=255, blank=False, null=False)
