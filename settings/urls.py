from django.conf.urls import url
from . import views as local_views

urlpatterns = [
    url('all_themes', local_views.AllThemes.as_view()),
    url('theme', local_views.ActiveTheme.as_view()),
]