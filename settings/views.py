from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist

from .models import Theme
from .models import UserTheme


# Create your views here.

class ActiveTheme(APIView):
    def get(self, request):
        user_id = str(request.user.id)
        try:
            user_theme = UserTheme.objects.get(user=user_id)
            active_theme = Theme.objects.filter(slug=user_theme.theme).values()
        except ObjectDoesNotExist:
            active_theme = Theme.objects.filter(slug='dark_theme').values()

        return Response({"success": True, "data": active_theme})

    def post(self, request):
        user_id = str(request.user.id)
        try:
            user_theme = UserTheme.objects.filter(user=user_id).first()
            user_theme.theme = request.data['slug']
            user_theme.save()
        except:
            UserTheme.objects.create(user=user_id, theme=request.data['slug'])
        return Response({"success": True})


class AllThemes(APIView):
    def get(self, request):
        all_themes = Theme.objects.all().values()
        return Response({"success": True, "data": all_themes})
