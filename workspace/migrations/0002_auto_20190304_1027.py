# Generated by Django 2.0.2 on 2019-03-04 10:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('workspace', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='workspace',
            old_name='workspace_name',
            new_name='name',
        ),
    ]
