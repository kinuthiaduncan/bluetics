from django.db import models
from django.contrib.postgres.fields import JSONField
# Create your models here.


class Workspace(models.Model):
    user = models.IntegerField(default=0)
    name = models.CharField(max_length=20, blank=True, null=True)
    content = JSONField()
    date_updated = models.DateTimeField(auto_now=True)
