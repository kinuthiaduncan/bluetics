from django.conf.urls import url
from django.urls import path
from . import views as local_views

urlpatterns = [
    path('<int:workspace_id>', local_views.WorkspaceDetails.as_view()),
    url('all', local_views.AllWorkspaces.as_view()),
    url('save', local_views.SaveWorkspace.as_view()),
    url('manage', local_views.ManageWorkspace.as_view()),
]
