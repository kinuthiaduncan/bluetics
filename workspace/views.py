from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Workspace

import datetime


class AllWorkspaces(APIView):
    def get(self, request):
        result = Workspace.objects.filter(user=request.user.id) \
            .order_by('-date_updated') \
            .values('id', 'name', 'content')
        return Response({"success": True, "result": result})


class WorkspaceDetails(APIView):
    def get(self, request, workspace_id):
        workspace = Workspace.objects.get(user=request.user.id, id=workspace_id)
        workspace.date_updated = datetime.datetime.now()
        workspace.save()

        result = {
            "id": workspace.id,
            "name": workspace.name,
            "content": workspace.content
        }

        return Response({"success": True, "result": result})


class SaveWorkspace(APIView):
    def post(self, request):
        content = request.data['input']
        workspace_id = content['id']
        workspace_content = content['content']
        try:
            entry = Workspace.objects.get(user=request.user.id, id=workspace_id)
            entry.content = workspace_content
            entry.save()
            return Response({"success": True})
        except Workspace.DoesNotExist:
            Workspace.objects.create(user=request.user.id, name="Autosave", content=workspace_content)
            return Response({"success": False})


class ManageWorkspace(APIView):
    def post(self, request):
        name = request.data['name']
        Workspace.objects.create(user=request.user.id, name=name, content={})

        return Response({"success": True})

    def delete(self, request):
        workspace_id = request.data['id']
        Workspace.objects.filter(user=request.user.id, id=workspace_id).delete()
        return Response({"success": True})
